/*
    GNU Octave level-set package.
    Copyright (C) 2013-2015  Daniel Kraft <d@domob.eu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Test program for heap, doing a simple heap sort of random numbers.  */

#include "Heap.hpp"

#include <cstdarg>
#include <cstdio>
#include <cstdlib>

using namespace fastMarching;

namespace fastMarching
{

/* Provide printf-based implementation of issueWarning.  */
void
issueWarning (const std::string&, const std::string& fmt, ...)
{
  va_list args;
  va_start (args, fmt);
  vprintf (fmt.c_str (), args);
  va_end (args);
}

} // namespace fastMarching

/**
 * Comparison routine used.  Simply compare doubles.
 * @param a First value.
 * @param b Second value.
 */
inline bool
compare (double a, double b)
{
  return a < b;
}

/** Type of heap.  */
typedef Heap<double, decltype (&compare)> heapT;

/**
 * Main routine.
 * @return Exit code 0.
 */
int
main ()
{
  heapT heap(&compare);

  for (int i = 0; i < 40000; ++i)
    {
      const double cur = static_cast<double> (std::rand ());
      heap.push (cur / RAND_MAX);
    }

  double last = **heap.top ();
  heap.pop ();

  while (!heap.empty ())
    {
      const double cur = **heap.top ();
      heap.pop ();

      if (cur > last)
        printf ("%f vs %f!\n", cur, last);
      last = cur;
    }

  return EXIT_SUCCESS;
}
