#   GNU Octave level-set package.
#   Copyright (C) 2013-2015  Daniel Kraft <d@domob.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Makefile to build the test files in C++ and the package's .oct files.

CXX ?= @CXX@
MKOCTFILE ?= mkoctfile

CXXFLAGS ?= -Wall -Wextra
FULL_CXXFLAGS = @CXXFLAGS@ $(CXXFLAGS)
EXTRA_FLAGS = -Weffc++ -pedantic -fPIC

MKOCT = CXXFLAGS="$(FULL_CXXFLAGS)" $(MKOCTFILE)

HEADERS = $(wildcard:*.hpp *.tpp)

ALGO_SOURCES = FastMarching.cpp
ALGO_OBJECTS = $(ALGO_SOURCES:%.cpp=%.o)

OCT_SOURCES = Utils.cpp
OCT_OBJECTS = $(OCT_SOURCES:%.cpp=%.o)

TESTS = heapsort marching

PREFIX = __levelset_
OCTS = internal_fastmarching internal_init_narrowband \
       geomElements geomBoundary geomGamma nbFromGeom \
       internal_mesh upwindGrad
OCT_FILES = $(OCTS:%=$(PREFIX)%.oct)

.PHONY: clean oct tests

oct: $(OCT_FILES)

tests: $(TESTS)

clean:
	rm -f *.o *.oct $(TESTS)

$(ALGO_OBJECTS): %.o: %.cpp $(HEADERS)
	$(CXX) $(FULL_CXXFLAGS) $(EXTRA_FLAGS) -c $< -o $@

$(OCT_OBJECTS): %.o: %.cpp $(HEADERS)
	$(MKOCT) -c $< -o $@

$(OCT_FILES): $(PREFIX)%.oct: %.cpp $(HEADERS) $(ALGO_OBJECTS) $(OCT_OBJECTS)
	$(MKOCT) $< $(ALGO_OBJECTS) $(OCT_OBJECTS) -o $@

$(TESTS): %: %.cpp $(HEADERS) $(ALGO_OBJECTS)
	$(CXX) $(FULL_CXXFLAGS) $(EXTRA_FLAGS) $< $(ALGO_OBJECTS) -o $@
