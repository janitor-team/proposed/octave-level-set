##  Copyright (C) 2014-2015  Daniel Kraft <d@domob.eu>
##  GNU Octave level-set package.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn  {Function File} {} ls_animate_evolution (@var{phi}, @var{f}, @var{h}, @var{times}, @var{wait})
## 
## Animate the evolution of a level-set geometry.  The evolution
## is calculated with @code{ls_solve_stationary} and @code{ls_extract_solution}
## for the given arguments,  and the result plotted in the current figure
## for the times given in @var{times}.  Between updating to the next
## ``movie frame'', sleep for @var{wait} seconds.
##
## @seealso{ls_solve_stationary, ls_extract_solution}
## @end deftypefn

function ls_animate_evolution (phi, f, h, times, wait)
  if (nargin () != 5)
    print_usage ();
  endif

  d = ls_solve_stationary (phi, f, h);

  % The figure is cleared and the speed plotted only once,
  % to prevent ugly "jittering".  At each step, only the
  % last contour plot object is removed to re-draw it.

  clf ();
  hold ("on");
  imagesc (f);
  ls_sign_colourmap ("highlight");
  set (gca (), "ydir", "normal");

  handle = false;
  for t = times
    phit = ls_extract_solution (t, d, phi, f);
    if (handle)
      delete (handle);
    endif
    [~, handle] = contour (phit, [0, 0], "k", "LineWidth", 2);
    axis ("equal");
    drawnow ();
    sleep (wait);
  endfor
  hold ("off");
endfunction

% Simple demo with a basic situation.
%!demo
%!  n = 100;
%!  x = linspace (-2, 2, n);
%!  h = x(2) - x(1);
%!  [XX, YY] = meshgrid (x, x);
%!
%!  phi = ls_genbasic (XX, YY, "sphere", [0, 0], 1.7);
%!  f = -XX.^2 - YY.^2;
%!
%!  times = linspace (0, 1, 60);
%!  ls_animate_evolution (phi, f, h, times, 0.05);
