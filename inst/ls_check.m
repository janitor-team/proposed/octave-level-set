##  Copyright (C) 2014  Daniel Kraft <d@domob.eu>
##  GNU Octave level-set package.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn  {Function File} {@var{ok} =} ls_check (@var{phi}, @var{type}, @var{where})
## 
## Check a geometric constraint for the level-set function @var{phi}.
##
## @var{type} specifies what the constraint
## should be, and @var{where} should be a logical array of the same size
## as the grid (and thus @var{phi}), specifying which grid points are
## part of the set that defines the constraint.
## Possible values for @var{type}:
##
## @table @asis
## @item @qcode{"inside"}
## The domain should be inside the region marked as @var{where}.
##
## @item @qcode{"outside"}
## The domain should not intersect the region marked as @var{where}.
##
## @item @qcode{"contain"}
## The domain should always contain the region marked in @var{where}.
## @end table
##
## @seealso{ls_enforce, ls_enforce_speed, ls_inside}
## @end deftypefn

function ok = ls_check (phi, type, where)
  if (nargin () ~= 3)
    print_usage ();
  endif

  if (~all (size (phi) == size (where)))
    error ("PHI and WHERE must be of the same size");
  endif
  if (~strcmp (typeinfo (type), "string"))
    error ("TYPE must be a string");
  endif

  inside = ls_inside (phi);
  switch (type)
    case "inside"
      ok = all (~inside(~where));
    case "outside"
      ok = all (~inside(where));
    case "contain"
      ok = all (inside(where));
    otherwise
      error ("invalid value '%s' for TYPE argument", type);
  endswitch
endfunction

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tests.

% Test for error conditions.
%!error <Invalid call to>
%!  ls_check (1, 2)
%!error <Invalid call to>
%!  ls_check (1, 2, 3, 4)
%!error <PHI and WHERE must be of the same size>
%!  ls_check (1, "inside", [1, 2]);
%!error <invalid value 'foo' for TYPE argument>
%!  ls_check (1, "foo", true);
%!error <TYPE must be a string>
%!  ls_check (1, NA, false);

% Basic tests for the cases.
%!test
%!  n = 100;
%!  x = linspace (-10, 10, n);
%!  [XX, YY] = meshgrid (x, x);
%!
%!  circ2 = (XX.^2 + YY.^2 < 2^2);
%!  circ8 = (XX.^2 + YY.^2 < 8^2);
%!  phi = (XX.^2 + YY.^2 - 5^2);
%!
%!  assert (ls_check (phi, "inside", circ8));
%!  assert (ls_check (phi, "contain", circ2));
%!  assert (ls_check (phi, "outside", ~circ8));
%!
%!  assert (~ls_check (phi, "inside", circ2));
%!  assert (~ls_check (phi, "contain", circ8));
%!  assert (~ls_check (phi, "outside", circ8));
